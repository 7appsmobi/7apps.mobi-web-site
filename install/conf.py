# -*- coding: utf-8 -*-
import os

PROJECT_NAME = 'cms'

INSTALLER_ROOT = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = os.path.dirname(INSTALLER_ROOT)
PY_PROJECT_ROOT = os.path.join(PROJECT_ROOT, PROJECT_NAME)

POSTGRES_HOST = 'localhost'
POSTGRES_ROOT_USER = 'dmitrii'
POSTGRES_ROOT_PASSWORD = ''

# Можно отменять/разрешать любой этап инсталяции
SKIP_VIRTUAL_ENV = False
SKIP_PIP = False
SKIP_MANAGEMENT_COMMANDS = False
