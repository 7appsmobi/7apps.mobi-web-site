import sys
from conf import *

os.chdir(PY_PROJECT_ROOT)
sys.path.append(PY_PROJECT_ROOT)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "web_cms.settings")

from django.conf import settings
from django.core import management

import django
django.setup()

folders_to_check = [settings.STATIC_ROOT,
                    settings.MEDIA_ROOT,]

for folder in folders_to_check:
    if not os.path.exists(folder):
        print 'creating %s' % folder
        os.mkdir(folder, 0777)

if not SKIP_MANAGEMENT_COMMANDS:
    if not settings.DEBUG:
        print "Calling collect static"
        management.call_command('collectstatic')

    print "Calling check db"
    management.call_command('db_maintenance', 'check_db')

    print "Calling migrate"
    management.call_command('migrate')
