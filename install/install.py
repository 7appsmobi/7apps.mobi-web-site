#!/usr/bin/python
# -*- coding: utf-8 -*-
import pprint
import shutil
from subprocess import call
import sys
import os

#
# Настройки
#

from conf import *

#
# ИНСТАЛЯТОР
#

print "project root is %s" % PROJECT_ROOT

ENV_PATH = os.path.join(PROJECT_ROOT, 'env')
if os.path.exists(ENV_PATH):
    print 'found virtual env at %s. overwrite?' % ENV_PATH
    answer = raw_input('yes/no? ')
    if answer != 'yes':
        SKIP_VIRTUAL_ENV = True
    else:
        shutil.rmtree(ENV_PATH)

if not SKIP_VIRTUAL_ENV:
    print 'installing virtualenv %s' % ENV_PATH
    call(['virtualenv', ENV_PATH, '-p', 'python2.7'])
else:
    print 'skipping virtual env'

ENV_BIN_PATH = os.path.join(ENV_PATH, 'bin')

if not SKIP_PIP:
    print 'updating modules'
    call([os.path.join(ENV_BIN_PATH, 'pip'), 'install', '-r', os.path.join(INSTALLER_ROOT, 'requirements.txt')])

log_dir = os.path.join(PROJECT_ROOT, 'logs')
if not os.path.exists(log_dir):
    print 'creating log dir %s' % log_dir
    os.mkdir(log_dir, 0777)

call([os.path.join(ENV_BIN_PATH, 'python'), os.path.join(INSTALLER_ROOT, 'install2.py')])
