"use strict"
var gulp = require('gulp'),
		sass = require('gulp-sass'),
		jade = require('gulp-jade'),
		livereload = require('gulp-livereload'),
		connect = require('gulp-connect'),
		prefix = require('gulp-autoprefixer'),
		watch = require('gulp-watch'),
		ftp = require('gulp-ftp'),
		concatCSS = require('gulp-concat-css'),
		rename = require('gulp-rename'),
		imagemin = require('gulp-imagemin'),
		cssbeautify = require('gulp-cssbeautify');

gulp.task('ftp', function () {
	return gulp.src('dist/**/*')
		.pipe(ftp({
			host: 'ftp.endy.nichost.ru',
			user: 'endy_ftp',
			pass: 'g4-kYAhJ',
			remotePath: '/endy.moscow/docs/'
		}));
});

gulp.task('img', function() {
	gulp.src('app/static/custom/img/**/*')
		.pipe(gulp.dest('dist/static/custom/img'))
});

gulp.task('js', function() {
	gulp.src('./app/static/custom/js/*')
	.pipe(gulp.dest('dist/static/custom/js/'))
	.pipe(livereload());
});

gulp.task('css', function() {
	gulp.src('app/static/css/*')
	.pipe(gulp.dest('dist/css'))
});

gulp.task('connect', function() {
	connect.server({
		root: 'dist',
		port: 1337,
		livereload: true
	});
});

gulp.task('sass', function () {
	gulp.src('./app/sass/style.scss')
		.pipe(sass())
		.pipe(prefix({
			browsers: ['last 3 versions'],
		}))
		.pipe(concatCSS('style.css'))
		.pipe(gulp.dest('dist/static/custom/css'))
		.pipe(livereload());
});

gulp.task('prefix', function () {
	gulp.src('./app/sass/animation-dev.scss')
		.pipe(prefix({
			browsers: ['last 10 versions'],
		}))
		.pipe(cssbeautify())
		.pipe(rename('animation.scss'))
		.pipe(gulp.dest('./app/sass/'))
});

gulp.task('jade', function () {
	gulp.src('./app/jade/index-dev.jade')
		.pipe(jade({
			basedir: './app/jade/',
			pretty: true
		}))
		.pipe(rename('index.html'))
		.pipe(gulp.dest('dist'))
		.pipe(livereload());
	gulp.src('./app/jade/about-dev.jade')
		.pipe(jade({
			basedir: './app/jade/',
			pretty: true
		}))
		.pipe(rename('about.html'))
		.pipe(gulp.dest('dist'))
		.pipe(livereload());
});

gulp.task('watch', function() {
	livereload.listen();
		gulp.watch(['./app/jade/*.jade'], ['jade'])
		gulp.watch(['./app/sass/*.scss'], ['sass'])
		gulp.watch(['./app/static/custom/js/*'], ['js'])
});

// Default
gulp.task('default', ['js', 'css', 'sass','jade','connect','watch']);
