function Timer(callback, delay) {
    this.timerId = null;
    this.start = null;
    this.remaining = delay;
    this.callback = callback;
    this.begin();
}

Timer.prototype.pause = function() {
    this.stop();
    this.remaining -= new Date() - this.start;
};

Timer.prototype.stop = function() {
    if (!this.timerId) {
        return;
    }

    window.clearTimeout(this.timerId);
    this.timerId = null;
};

Timer.prototype.begin = function() {
    this.start = new Date();
    this.timerId = window.setTimeout(this.callback, this.remaining);
};

Timer.prototype.resume = function() {
    if (this.timerId) {
        return;
    }

    this.begin();
};


function Plank($parentElement) {
    this.$el = $parentElement;
    this.init();
    this.paused = true;
    this.imagesLoaded = false;
    this.pauseDelayTimer = null;
}

Plank.prototype.init = function() {
    var self = this;

    self.animatedElements = this.$el.find('.pause');
    self.$el.imagesLoaded({ background: true }).always(function() {
        self.imagesLoaded = true;
        if (self.isInViewport()) {
            self.resume();
        }
    });
};

Plank.prototype.isPaused = function() {
    return this.paused;
};

Plank.prototype.pause = function() {
    if (this.isPaused()) {
        return;
    }

    this.animatedElements.addClass('pause');
    this.paused = true;
    this.abortDelayedPause();
};

Plank.prototype.delayedPause = function() {
    var self = this;

    if (this.isPaused() || this.pauseDelayTimer) {
        return;
    }

    this.pauseDelayTimer = setTimeout(function() {
        if (self.isInViewport()) {
            self.abortDelayedPause();
            return;
        }

        self.pause();
    }, 10000);
};

Plank.prototype.abortDelayedPause = function() {
    if (this.pauseDelayTimer) {
        clearTimeout(this.pauseDelayTimer);
        this.pauseDelayTimer = null;
    }
};

Plank.prototype.resume = function() {
    if (!this.isPaused() || !this.imagesLoaded) {
        return;
    }

    this.abortDelayedPause();
    this.animatedElements.removeClass('pause');
    this.paused = false;
};

Plank.prototype.isInViewport = function() {
    var rect = this.$el[0].getBoundingClientRect();

    return (
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.bottom > 0
    );
};


function InsanityPlank($parentElement) {
    this.sceneTimer = null;
    this.current_animation = {};
    this.heroes = [];
    this.cops = [];
    this.herobox = null;
    this.copbox = null;

    Plank.apply(this, arguments);
}
InsanityPlank.prototype = Object.create(Plank.prototype);
InsanityPlank.prototype.constructor = Plank;

InsanityPlank.prototype.pause = function() {
    //if (this.isPaused()) {
    //    return;
    //}
    //
    //Plank.prototype.pause.apply(this, arguments);
    //this.clearAndPrepareScene();
};

InsanityPlank.prototype.resume = function() {
    if (!this.isPaused()) {
        return;
    }

    Plank.prototype.resume.apply(this, arguments);
    this.startScene();
};

InsanityPlank.prototype.nextScene = function() {
    var self = this;

    self.heroes.some(function(hero, i) {
        if (hero == self.current_animation.hero) {
            var heroIndex = i + 1;
            if (heroIndex == self.heroes.length) {
                heroIndex = 0;
            }

            self.current_animation = {
                'hero': self.heroes[heroIndex],
                'cop': self.cops[heroIndex % 2]
            };

            return true;
        }
    });
};

InsanityPlank.prototype.clearAndPrepareScene = function() {
    if (this.sceneTimer) {
        this.sceneTimer.stop();
    }

    this.current_animation.hero.removeClass('appearing standby alerted running').addClass('hidden');
    this.current_animation.cop.removeClass('appearing running').addClass('hidden');

    this.herobox.removeClass('hero-fly');
    this.copbox.removeClass('cop-fly');

    this.nextScene();
};

InsanityPlank.prototype.startScene = function() {
    var self = this;

    self.current_animation.hero.removeClass('hidden').addClass('appearing');

    self.sceneTimer = new Timer(function() {
        self.current_animation.hero.removeClass('appearing').addClass('standby');

        self.sceneTimer = new Timer(function() {
            self.current_animation.cop.removeClass('hidden').addClass('appearing');

            self.sceneTimer = new Timer(function() {
                self.current_animation.hero.removeClass('standby').addClass('alerted');

                self.sceneTimer = new Timer(function() {
                    self.current_animation.cop.removeClass('appearing').addClass('running');
                    self.copbox.addClass('cop-fly');

                    self.sceneTimer = new Timer(function() {
                        self.current_animation.hero.removeClass('alerted').addClass('running');
                        self.herobox.addClass('hero-fly');

                        self.sceneTimer = new Timer(function() {
                            self.clearAndPrepareScene();
                            self.startScene();
                        }, 4000); // пауза между началом убегания героя и началом новой сцены
                    }, 150); // пауза между началом погони копа и началом убегания героя
                }, 2000); // пауза между встревоженностью персонажа и началом погони копа
            }, 1000); // пауза между появлением копа и встревоженностью персонажа
        }, 2000); // пауза между концом появления героя и появлением копа
    }, 3000); // пауза между началом анимации и концом появления
};

InsanityPlank.prototype.init = function() {
    Plank.prototype.init.apply(this, arguments);

	var $cloudsInsanity = this.$el.find('.cloud1-insanity, .cloud2-insanity, .cloud3-insanity, .cloud4-insanity');
	$cloudsInsanity.bind('animationend', function(){
		$(this).unbind('animationend').addClass('cloud-insanity-infinite');
	});

	var self = this,
        current_hero = Math.floor(Math.random() * 4) ;

    ['.hero1', '.hero2', '.hero3', '.hero4'].forEach(function(css) {
        self.heroes.push(self.$el.find(css));
    });

    ['.cop1', '.cop2'].forEach(function(css) {
        self.cops.push(self.$el.find(css));
    });

    self.herobox = this.$el.find('.herobox');
    self.copbox = this.$el.find('.copbox');

    self.current_animation = {
        'hero': self.heroes[current_hero],
        'cop': self.cops[current_hero % 2]
    };

	self.clearAndPrepareScene();
};


function LittleChefPlank($parentElement) {
    Plank.apply(this, arguments);
}
LittleChefPlank.prototype = Object.create(Plank.prototype);
LittleChefPlank.prototype.constructor = Plank;

LittleChefPlank.prototype.init = function() {
    Plank.prototype.init.apply(this, arguments);

    var $cloudsBig = $('.cloud-big-lc, .cloud-big2-lc, .cloud-big3-lc'),
		$cloudsSmall = $('.cloud-small-lc, .cloud-small2-lc, .cloud-small3-lc, .cloud-small4-lc, .cloud-small5-lc, .cloud-small6-lc');

	$cloudsBig.bind('animationend', function(){
		$(this).unbind('animationend').addClass('cloud-infinite-play-big-lc');
	});

	$cloudsSmall.bind('animationend', function(){
		$(this).unbind('animationend').addClass('cloud-infinite-play-small-lc');
	});
};

$(function() {
    var insanityPlank = new InsanityPlank($('#insanity')),
        lcPlank = new LittleChefPlank($('#littlechef')),
        cosmicPlank = new Plank($('#cosmic'));

    var planks = [insanityPlank, lcPlank, cosmicPlank],
        check_planks = function() {
            planks.forEach(function(plank) {
                if (plank.isInViewport()) {
                    plank.resume();
                } else {
                    plank.delayedPause();
                }
            });
        };

    if (insanityPlank.$el.length + lcPlank.$el.length + cosmicPlank.$el.length) {
        $(window).bind('load resize scroll', check_planks);
        check_planks();
    }
});


// GOOGLE ANALYTICS
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-68520327-2', 'auto');
ga('require', 'linkid');
ga('send', 'pageview');


// SUBSCRIBE
$(function() {
    var $subscribe_form = $(".subscribe-form form");
    if (localStorage.getItem("subscribed")) {
        $subscribe_form.hide();
    } else {
        $subscribe_form.submit(function(ev){
            ev.preventDefault();
            var email = $subscribe_form.find("input[type=email]").val();
            if (email == "") {
                return false;
            }
            $.post(
              $subscribe_form.attr('action'),
              $subscribe_form.serializeArray(),
              function(json_data){
                  $(".subscribe-form .info")
                      .text(json_data.content)
                      .removeClass()
                      .addClass("info")
                      .addClass(json_data.class);
                  if (json_data.class == "success"){
                      localStorage.setItem('subscribed', true);
                  }
              }
            );
        });
    }
});

// Для адекватного перехода по ссылкам в случае открытия страницы в iframe
$(function() {
    if (window!=window.top) {
        $(this).contents().find("a").each(function() {
            $(this).attr('target', '_top');
        });
    }
});