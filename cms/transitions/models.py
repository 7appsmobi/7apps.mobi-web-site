# -*- coding: utf-8 -*-
from cms.models import Page
from django.db import models

from games.models import Game


class Transition(models.Model):
    game = models.ForeignKey(Game)
    added = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"{} . {}".format(self.added.strftime('%x'), self.game)

    class Meta:
        verbose_name = u'Переход'
        verbose_name_plural = u'Переходы'