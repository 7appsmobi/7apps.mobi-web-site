# coding=utf-8
import json

from django.contrib import admin
from django.contrib.admin.options import csrf_protect_m
from django.db.models import Count
from models import Transition
from transitions.postgres_func import DateTrunc


class MyEntryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/transition__change_list.html'

    @csrf_protect_m
    def changelist_view(self, request, extra_context=None):
        qs = self.model.objects.annotate(date=DateTrunc('added', 'day'))
        qs = qs.values('game__title', 'date').annotate(count=Count('id')).order_by('date')
        titles = list(set(t['game__title'] for t in qs))
        googlechart_header = [['Date'] + titles, ]

        # Запоминаем порядок игр в будущем графике
        games_priority = {}
        for index, title in enumerate(titles):
            games_priority[title] = index
        # Собираем информацию в словарь вида {27.06.1993: [77, 231], 29.06.1993: [190, 452], ...}
        dct_for_googlechart_table = {}
        for day_info in qs:
            date = day_info['date']
            title = day_info['game__title']
            index = games_priority[title]
            if date not in dct_for_googlechart_table:
                dct_for_googlechart_table[date] = [0 for x in range(len(titles))]
            dct_for_googlechart_table[date][index] = day_info['count']
        # Сортируем по датам и приводим к виду: [["27.06.1993", 77, 231], ["29.06.1993", 190, 452], ...]
        googlechart_table = []
        for key in sorted(dct_for_googlechart_table.keys()):
            row = [key.strftime('%x')] + dct_for_googlechart_table[key]
            googlechart_table.append(row)

        googlechart_data = googlechart_header + googlechart_table
        if extra_context:
            extra_context.update({'googlechart_data': json.dumps(googlechart_data)})
        else:
            extra_context = {'googlechart_data': json.dumps(googlechart_data)}
        return super(MyEntryAdmin, self).changelist_view(request, extra_context)

admin.site.register(Transition, MyEntryAdmin)
