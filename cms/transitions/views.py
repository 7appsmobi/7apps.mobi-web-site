# -*- coding: utf-8 -*-
from django.shortcuts import redirect
from .models import Transition
from games.models import Game


def add_visitor(request, *args, **kwargs):
    game = Game.objects.filter(slug=kwargs.get('game_slug')).first()
    if game:
        Transition.objects.create(game=game).save()
        return redirect(game.get_appstore_url())
    return redirect('/')
