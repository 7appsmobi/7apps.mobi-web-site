# coding=utf-8
import pprint

import pyjade
from django import template
from django.conf import settings
from django.core.urlresolvers import reverse

from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag()
def appstore_button(game_slug):
    url = reverse('add_visitor', kwargs={'game_slug': game_slug})
    return render_to_string('parts/appstore_button.jade',
                     {'url': url
                      })


@pyjade.register_filter('appstore_button')
def appstore_button_jade(text, dct):
    game_slug = dct.get('game_slug')
    url = reverse('add_visitor', kwargs={'game_slug': game_slug})
    return render_to_string('parts/appstore_button.jade',
                     {'url': url
                      })
