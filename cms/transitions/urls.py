# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
    url(r'^(?P<game_slug>.+)/?$', views.add_visitor, name="add_visitor"),
)