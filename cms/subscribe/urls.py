from django.conf.urls import patterns, url
from . import views


urlpatterns = patterns('',
    url(r'^$', views.add_subscriber, name="add_subscriber"),
)