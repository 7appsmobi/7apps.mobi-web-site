# coding=utf-8
from models import Subscriber
from django.contrib import admin


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    list_display = ['email', 'added', ]
