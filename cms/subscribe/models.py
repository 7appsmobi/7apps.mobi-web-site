# -*- coding: utf-8 -*-

from django.db import models


class Subscriber(models.Model):
    email = models.CharField(max_length=128)
    added = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"Email: {}; Added: {}".format(self.email, self.added)

    class Meta:
        verbose_name = u'Подписчик'
        verbose_name_plural = u'Подписчики'
