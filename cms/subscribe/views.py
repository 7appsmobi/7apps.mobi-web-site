# -*- coding: utf-8 -*-
from django.http import JsonResponse
from subscribe.models import Subscriber


def add_subscriber(request):
    email = request.POST.get("email")
    if email and u"@" in email and u"." in email:
        if Subscriber.objects.filter(email=email).exists():
            return JsonResponse({'content': 'You are already subscribed :)',
                                 'class': 'success'})
        else:
            try:
                Subscriber(email=email).save()
                return JsonResponse({'content': 'Subscribed!',
                                     'class': 'success'})
            except Exception:
                return JsonResponse(
                                    {'content': 'An error has occurred.',
                                     'class': 'error'
                                     })
    return JsonResponse(
        {'content': 'Wrong email.',
         'class': 'error'})
