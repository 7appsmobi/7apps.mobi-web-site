from django.conf import settings
from django.shortcuts import get_object_or_404, render_to_response
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.clickjacking import xframe_options_exempt, xframe_options_deny, xframe_options_sameorigin
from django.views.generic import View


class InsanityView(View):
    @xframe_options_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(InsanityView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render_to_response("insanity.jade", {
            "STATIC_URL": settings.STATIC_URL
        })
