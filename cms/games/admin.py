# coding=utf-8
from django.contrib import admin
from games.models import Game


@admin.register(Game)
class GamesAdmin(admin.ModelAdmin):
    list_display = ['title', 'appstore_link', 'slug']
