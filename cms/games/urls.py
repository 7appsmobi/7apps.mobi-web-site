from django.conf.urls import patterns, url
from games.insanity import InsanityView


urlpatterns = patterns('',
    url(r'^insanity/replay', InsanityView.as_view(), name="insanity_view"),
)