# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify


class Game(models.Model):
    title = models.CharField(max_length=64)
    appstore_link = models.CharField(max_length=128)
    slug = models.CharField(max_length=128, null=True, blank=True)

    def __unicode__(self):
        return u"{}".format(self.title)

    def get_appstore_url(self):
        return self.appstore_link or u""

    class Meta:
        verbose_name = u'Игра'
        verbose_name_plural = u'Игры'


@receiver(post_save, sender=Game)
def create_slug(sender, instance, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)
        instance.save()