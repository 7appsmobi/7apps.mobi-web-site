# coding=utf-8
from cms.admin.placeholderadmin import FrontendEditableAdminMixin
from django.contrib import admin
from models import Review


@admin.register(Review)
class ReviewAdmin(FrontendEditableAdminMixin, admin.ModelAdmin):
    list_display = ['game', 'title', 'stars_count', 'text', 'language', ]
    list_filter = ['language', 'game', ]
    frontend_editable_fields = ['title', 'stars_count', 'text', 'author', ]
