# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game', models.CharField(blank=True, max_length=5, null=True, choices=[(b'INS', b'Insanity'), (b'LC', b'Little chef')])),
                ('title', models.CharField(max_length=60, null=True, blank=True)),
                ('text', models.CharField(max_length=250, null=True, blank=True)),
                ('stars_count', models.FloatField(null=True, blank=True)),
                ('author', models.CharField(max_length=25, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u041e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
    ]
