# -*- coding: utf-8 -*-
import math

from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver


class Review(models.Model):
    INS = 'INS'
    LC = 'LC'
    GAMES = (
        (INS, 'Insanity'),
        (LC, 'Little chef')
    )
    game = models.CharField(max_length=5, choices=GAMES)
    title = models.CharField(max_length=60, null=True, blank=True)
    text = models.TextField(max_length=250, null=True, blank=True)
    stars_count = models.FloatField(null=True)
    author = models.CharField(max_length=25)
    language = models.CharField(max_length=5, choices=settings.LANGUAGES)

    def __unicode__(self):
        return u"Game: {}; Language: {}; Title: {}".format(self.get_game_display(), self.language, self.title)

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'


@receiver(pre_save, sender=Review)
def stars_round(sender, instance, **kwargs):
    if instance.stars_count < 0:
        instance.stars_count = 0
    if instance.stars_count > 5:
        instance.stars_count = 5
    if instance.stars_count * 10 % 10 > 5:
        instance.stars_count = math.ceil(instance.stars_count/0.5)*0.5
    else:
        instance.stars_count = math.floor(instance.stars_count/0.5)*0.5
