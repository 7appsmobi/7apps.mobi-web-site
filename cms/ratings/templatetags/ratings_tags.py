# coding=utf-8
from sass_patch import SassFileStoragePatched

from ratings.models import Review
from django import template
from django.conf import settings
from django.template.loader import get_template
from django.utils.translation import get_language
from sass_processor.templatetags.sass_tags import SassSrcNode

register = template.Library()


@register.simple_tag(name='show_ratings', takes_context=True)
def show_ratings(context, game):
    ratings = Review.objects.filter(game=game, language=get_language()).order_by('?')[:5]
    context.update({
        "ratings": ratings
    })
    return get_template('parts/rating.jade').render(context)


@register.simple_tag(name='get_stars_classes_list')
def get_stars_classes_list(review, one="star-on", semi="star-semi", off="star-off"):
    result = ["star-on" for star in range(int(review.stars_count))]
    if review.stars_count - int(review.stars_count):
        result.append("star-semi")
    for i in range(5 - len(result)):
        result.append("star-off")
    return get_template('parts/stars.jade').render({
        "stars": result
    })