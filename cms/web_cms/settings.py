import platform
from logs import init_logging
from local_settings import update_settings
import os

gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))

"""
Django settings for web_cms project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PARENT_DIR = os.path.dirname(BASE_DIR)
PROJECT_ROOT = PARENT_DIR

BACKUP_ROOT = os.path.join(PARENT_DIR, 'backups')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '3ch1isk!-s27rxa_=@0&n%7(juw@qqcq3y$rr++^cj04r-hb1^'

# SECURITY WARNING: don't run with debug turned on in production!

developers = ('ws-scham', 'ws-dev')
if platform.system() == 'Darwin' or platform.uname()[1] in developers:
    PRODUCTION = False
else:
    PRODUCTION = True

DEBUG = not PRODUCTION

ALLOWED_HOSTS = []


# Application definition

ROOT_URLCONF = 'web_cms.urls'

WSGI_APPLICATION = 'web_cms.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(PARENT_DIR, 'media/')
STATIC_ROOT = os.path.join(PARENT_DIR, 'static/')

SITE_ID = 1

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': (
        os.path.join(PARENT_DIR, 'source', 'app', 'jade'),
        os.path.join(BASE_DIR, 'web_cms', 'templates'),
        os.path.join(BASE_DIR, 'transitions', 'templates'),

    ),
    'OPTIONS': {
        'context_processors': [
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
            'django.core.context_processors.i18n',
            'django.core.context_processors.debug',
            'django.core.context_processors.request',
            'django.core.context_processors.media',
            'django.core.context_processors.csrf',
            'django.core.context_processors.tz',
            'sekizai.context_processors.sekizai',
            'django.core.context_processors.static',
            'cms.context_processors.cms_settings',
        ],
        'loaders': [
            ('pyjade.ext.django.Loader', (
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            )),
        ],
        'builtins': ['pyjade.ext.django.templatetags'],
    }
}]

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
)

INSTALLED_APPS = (
    'maintenance',
    'djangocms_admin_style',
    'sass_processor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_text_ckeditor',
    'djangocms_style',
    'djangocms_column',
    'djangocms_file',
    'djangocms_flash',
    'djangocms_googlemap',
    'djangocms_inherit',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_teaser',
    'djangocms_video',
    'reversion',
    'sass_patch',
    'adminplusplus',
    'games',
    'ratings',
    'subscribe',
    'transitions',
    'debug_toolbar'
)

LANGUAGES = (
    ## Customize this
    ('ru', gettext('ru')),
    ('en', gettext('en')),
)

# CMS_LANGUAGES = {
#     ## Customize this
#     'default': {
#         'public': True,
#         'hide_untranslated': False,
#         'redirect_on_fallback': True,
#     },
#     1: [
#         {
#             'public': True,
#             'code': 'ru',
#             'hide_untranslated': False,
#             'name': gettext('ru'),
#             'redirect_on_fallback': True,
#         },
#         {
#             'public': True,
#             'code': 'en',
#             'hide_untranslated': False,
#             'name': gettext('en'),
#             'redirect_on_fallback': True,
#         },
#     ],
# }

LOCALE_PATHS = [
    os.path.join(PARENT_DIR, 'locale'),
]

CMS_TEMPLATES = (
    ('index.jade', 'Index page',),
    ('inner.jade', 'Inner page',),
    # ('inner.html', 'Inner page html',),
)

CMS_PERMISSION = True

CMS_PLACEHOLDER_CONF = {}

DATABASES = {
    'master': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': '777',
        'HOST': 'localhost'
    },
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'web7appsmobi',
        'USER': 'postgres',
        'PASSWORD': '777',
        'HOST': 'localhost'
    }
}

DATA_ROOT = os.path.join(PARENT_DIR, 'data/')

LOG_ROOT = os.path.join(PARENT_DIR, 'logs/')
LOGGING = init_logging(LOG_ROOT)

if not PRODUCTION:
    HOST = 'localhost'
else:
    HOST = os.path.split(PARENT_DIR)[1]

update_settings(globals())

CMS_STATIC_ROOT = os.path.join(PARENT_DIR, 'source', 'app', 'static')

SASS_ROOT = os.path.join(PARENT_DIR, 'source', 'app', 'sass')
SASS_PROCESSOR_ROOT = os.path.join(STATIC_ROOT, 'custom', 'css')
SASS_PROCESSOR_ENABLED = True

STATICFILES_DIRS = (
    CMS_STATIC_ROOT,
    SASS_ROOT,
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'sass_processor.finders.CssFinder',
)

if DEBUG:
    STATICFILES_FINDERS += (
        'sass_patch.StaticFinder',
    )

    DEBUG_TOOLBAR_CONFIG = {
       'SHOW_TOOLBAR_CALLBACK': 'maintenance.debug.show_toolbar_superuser'
    }

    from maintenance.debug import add_debug_panel
    add_debug_panel(globals())

ALLOWED_HOSTS += (HOST, 'beta.7apps.mobi', )
