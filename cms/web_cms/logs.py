import logging, copy

import os
from django.utils.log import DEFAULT_LOGGING


def suppress_deprecated_filter(record):
        warnings_to_suppress = [
            'RemovedInDjango19Warning'
        ]
        # Return false to suppress message.
        return not any([warn in record.getMessage() for warn in warnings_to_suppress])


def init_logging(log_root):
    log_file = os.path.join(log_root, 'django.log')
    if not os.path.exists(log_file):
        open(log_file, 'a').close()

    return {
        'version': 1,
        'disable_existing_loggers': True,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            },
            'skip_deprecated': {
                '()': 'django.utils.log.CallbackFilter',
                'callback': suppress_deprecated_filter,
            },
        },
        'formatters': {
            'verbose': {
                'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'handlers': {
            'file': {
                'level': 'WARNING',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': log_file,
                'maxBytes': 1024*1024*10,
                'backupCount': 3,
                'formatter': 'verbose',
                'filters': ['skip_deprecated', 'require_debug_false', ]
            },
            'console': {
                'level': 'INFO',
                'filters': ['skip_deprecated'],
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['file', 'console', ],
                'propagate': True,
                'level': 'WARNING',
            },
        }
    }