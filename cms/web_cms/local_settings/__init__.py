import importlib


def update_settings(global_settings):
    local_settings_filename = global_settings['HOST'].replace(".", "_").replace("-", "_")
    local_settings = importlib.import_module('web_cms.local_settings.%s' % local_settings_filename)
    local_settings.update(global_settings)
