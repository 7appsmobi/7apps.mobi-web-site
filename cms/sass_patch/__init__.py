from django.conf import settings
from sass_processor.storage import SassFileStorage
from django.core.files.storage import FileSystemStorage
from django.contrib.staticfiles.finders import BaseStorageFinder


class SassFileStoragePatched(SassFileStorage):
    def __init__(self, location=None, base_url=None, *args, **kwargs):
        if location is None:
            location = getattr(settings, 'SASS_PROCESSOR_ROOT', settings.STATIC_ROOT)
        if base_url is None:
            base_url = settings.STATIC_URL
        super(SassFileStorage, self).__init__(location, base_url, *args, **kwargs)


# noinspection PyAbstractClass
class StaticFinder(BaseStorageFinder):
    storage = FileSystemStorage(settings.STATIC_ROOT, settings.STATIC_URL)
