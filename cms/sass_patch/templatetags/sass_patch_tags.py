from sass_patch import SassFileStoragePatched
from django import template
from django.conf import settings
from sass_processor.templatetags.sass_tags import SassSrcNode

register = template.Library()


class SassSrcLink(SassSrcNode):
    def __init__(self, path):
        super(SassSrcLink, self).__init__(path)
        self.storage = SassFileStoragePatched()
        self.prefix = "%scustom/css/" % settings.STATIC_URL

    def render(self, context):
        href = super(SassSrcLink, self).render(context=context)
        return '<link href="%s" rel="stylesheet" />' % href


@register.tag(name='sass_link')
def sass_link(parser, token):
    return SassSrcLink.handle_token(parser, token)